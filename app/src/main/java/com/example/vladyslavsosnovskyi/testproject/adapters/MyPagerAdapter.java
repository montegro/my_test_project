package com.example.vladyslavsosnovskyi.testproject.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.vladyslavsosnovskyi.testproject.fragments.MakeOrderFragment;
import com.example.vladyslavsosnovskyi.testproject.fragments.OrderListFragment;


public class MyPagerAdapter extends FragmentPagerAdapter {

    private final int PAGE_COUNT = 2;

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return MakeOrderFragment.newInstance();
            case 1:
                return OrderListFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

}
