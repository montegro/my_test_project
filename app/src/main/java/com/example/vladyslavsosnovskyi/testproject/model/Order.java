package com.example.vladyslavsosnovskyi.testproject.model;


import java.util.concurrent.ThreadLocalRandom;

public class Order {

    String id;
    Integer int_id;
    String address;
    Integer price;
    Integer number;

    public Order(String id, Integer int_id, String address, Integer price) {
        this.id = id;
        this.int_id = int_id;
        this.address = address;
        this.price = price;
        this.number = getRandomNumber(100000, 999999);
    }

    public Order() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getInt_id() {
        return int_id;
    }

    public void setInt_id(Integer int_id) {
        this.int_id = int_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getNumber() {
        return number;
    }

    private int getRandomNumber(int min, int max) {
        int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
        return randomNum;
    }
}
