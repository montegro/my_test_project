package com.example.vladyslavsosnovskyi.testproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vladyslavsosnovskyi.testproject.R;
import com.example.vladyslavsosnovskyi.testproject.commons.Utils;
import com.example.vladyslavsosnovskyi.testproject.model.Order;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class MakeOrderFragment extends Fragment {

    private static int int_id = 0;
    Button btn_make_order;
    EditText et_address;
    EditText et_price;
    DatabaseReference mDataBase;

    public MakeOrderFragment() {
    }

    public static MakeOrderFragment newInstance() {
        MakeOrderFragment fragment = new MakeOrderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_make_order, container, false);
        Utils.setupUI(v, getActivity());
        mDataBase = FirebaseDatabase.getInstance().getReference();
        btn_make_order = v.findViewById(R.id.btn_send);
        et_address = v.findViewById(R.id.et_address);
        et_price = v.findViewById(R.id.et_price);

        btn_make_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addOrder();
            }
        });
        return v;
    }

    private void addOrder() {
        String address = et_address.getText().toString().trim();
        try {
            Integer price = Integer.parseInt(et_price.getText().toString().trim());
            if (!TextUtils.isEmpty(address)) {
                String id = mDataBase.child("ongoing_orders").push().getKey();
                Order order = new Order(id, int_id, address, price);
                int_id++;
                mDataBase.child("ongoing_orders").child(id).setValue(order);
                Toast.makeText(getContext(), "Заказ добавлен", Toast.LENGTH_SHORT).show();
                clearInput();
            }
        } catch (NumberFormatException e) {
            Toast.makeText(getContext(), "Введите корректную сумму заказа", Toast.LENGTH_SHORT).show();
        }
    }

    private void clearInput() {
        et_price.setText("");
        et_address.setText("");
    }

}
