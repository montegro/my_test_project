package com.example.vladyslavsosnovskyi.testproject.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.vladyslavsosnovskyi.testproject.R;
import com.example.vladyslavsosnovskyi.testproject.adapters.MyPagerAdapter;


public class OrdersMainFragment extends Fragment {

    BottomNavigationView mBottomNavigationView;
    ViewPager mViewPager;

    public OrdersMainFragment() {
    }


    public static OrdersMainFragment newInstance() {
        OrdersMainFragment fragment = new OrdersMainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_orders_main, container, false);
        mViewPager = v.findViewById(R.id.viewpager);
        mBottomNavigationView = v.findViewById(R.id.bottom_navigation);
        mViewPager.setAdapter(new MyPagerAdapter(getActivity().getSupportFragmentManager()));
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.bn_orders:
                        mViewPager.setCurrentItem(0);
                        return true;
                    case R.id.bn_list:
                        mViewPager.setCurrentItem(1);
                        return true;
                    default:
                        return false;
                }
            }
        });
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mBottomNavigationView.setSelectedItemId(R.id.bn_orders);
                        break;
                    case 1:
                        mBottomNavigationView.setSelectedItemId(R.id.bn_list);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return v;
    }

}
